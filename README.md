# Jitsi Branding

## Environment variables

Configuration is made using following environment variables:

- `BACKGROUND_COLOR`: hex value for the color used as background
- `BACKGROUND_IMAGE`: url for the image used as background
- `LOGO_URL`: anchor url used when clicking the logo image
- `LOGO_IMAGE`: url used for the image used as logo

## License

This program is free software and is distributed under [AGPLv3+ License](./LICENSE).
