#!/bin/sh

GENERATED_FILE=/app/config.js
HAS_CONTENT=0

echo "{" > "${GENERATED_FILE}"

# The hex value for the color used as background
if [ -n "${BACKGROUND_COLOR}" ]; then
  HAS_CONTENT=1
  echo "  \"backgroundColor\": \"${BACKGROUND_COLOR}\"," >> "${GENERATED_FILE}"
fi

# The url for the image used as background
if [ -n "${BACKGROUND_IMAGE}" ]; then
  HAS_CONTENT=1
  echo "  \"backgroundImageUrl\": \"${BACKGROUND_IMAGE}\"," >> "${GENERATED_FILE}"
fi

# The anchor url used when clicking the logo image
if [ -n "${LOGO_URL}" ]; then
  HAS_CONTENT=1
  echo "  \"logoClickUrl\": \"${LOGO_URL}\"," >> "${GENERATED_FILE}"
fi

# The url used for the image used as logo
if [ -n "${LOGO_IMAGE}" ]; then
  HAS_CONTENT=1
  echo "  \"logoImageUrl\": \"${LOGO_IMAGE}\"," >> "${GENERATED_FILE}"
fi

# remove last comma
if [ "${HAS_CONTENT}" = "1" ]; then
  sed -i '$ s/,$//g' "${GENERATED_FILE}"
fi

echo "}" >> "${GENERATED_FILE}"
