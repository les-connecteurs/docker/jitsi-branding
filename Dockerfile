FROM nginx:1.19.6-alpine

EXPOSE 80

RUN mkdir -p /app

COPY ./default.conf /etc/nginx/conf.d/default.conf
COPY ./init.sh /docker-entrypoint.d/50-init-config.sh
RUN chmod +x /docker-entrypoint.d/50-init-config.sh
